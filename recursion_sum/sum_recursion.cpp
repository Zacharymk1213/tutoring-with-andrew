#include <iostream>

using namespace std;
//Sum of Digits: Write a recursive function to calculate the sum of the digits of a positive integer n. For example, the sum of digits of 123 would be 1 + 2 + 3 = 6.

int sum(int num) {
	if (num==1) {
		return 1;
	}

	return num+=sum(num-1);


}


int main() {
	cout << "Input a number above 0 to get the sum of the digits up to and before it:";
	int x;
	cin >> x;
	cout << endl;
	cout << sum(x) << endl;
	return 0;
}
