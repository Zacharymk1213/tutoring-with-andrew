#include <iostream>

//Factorial: Write a recursive function to calculate the factorial of a given number n. The factorial of n is the product of all positive integers less than or equal to n. For example, the factorial of 5 (denoted as 5!) is 5 * 4 * 3 * 2 * 1 = 120.
using namespace std;

int factorial(int number) {
	if (number==1) {
		return 1;
	}


	return number*=factorial(number-1);


}

int main(){
	cout << "Enter the number to calculate the factorial of:";
	int x;
	cin >> x;
	cout << "Here's the factorial:" << factorial(x) << endl;

	return 0;

}
