#include <iostream>
using namespace std;

//    Write a recursive function to calculate the sum of the digits of a positive integer.

int sum_pos(int number) {
	if ((number/10)< 1) {
		return number%10;
	}
	return (number%10)+sum_pos(number/10);


}


int main(){

	cout << "Input a number to calculat the sum of the digits of:" << endl;
	int x;
	cin >> x;
	cout << "The number is:" << sum_pos(x) << endl;
	return 0;
}
