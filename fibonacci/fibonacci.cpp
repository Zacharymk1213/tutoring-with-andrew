#include <iostream>
using namespace std;
///Fibonacci Sequence: Write a recursive function to calculate the nth number in the Fibonacci sequence. In the Fibonacci sequence, each number is the sum of the two preceding ones.
// The sequence starts with 0 and 1. So, the first few numbers in the Fibonacci sequence are: 0, 1, 1, 2, 3, 5, 8, 13, ...


int fibonacci(int number) {
	if (number==0) {
		return 0;
	}
	if (number==1) {
		return 1;
	}
	return  number+=((fibonacci(number-1))+=fibonacci(number-2));

}

int main(){
	cout << "Get a fibonacci number:";
	int x;
	cin >> x;
	cout << "Here's the fibonacci number:" << fibonacci(x) << endl;
	return 0;
}
